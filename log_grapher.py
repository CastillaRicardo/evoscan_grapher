""" Lazy Module String """
#!/usr/bin/env python

from __future__ import print_function
import csv
import wx
import matplotlib.pyplot as plt

class MainWindow(wx.ScrolledWindow):
    """ Main Window of Program """
    def __init__(self, parent):
        self.parent = parent
        self.dirname = ''
        self.filename = ''
        self.checkboxes = {}
        self.reader = None

        # Call parent constuctor
        wx.ScrolledWindow.__init__(self, parent, -1, style=wx.TAB_TRAVERSAL)

        grid = wx.GridBagSizer(vgap=0, hgap=3)
        self.sizer = grid

        # setup buttons
        self._load_btn = wx.Button(self, -1, "Load Log")
        self._graph_btn = wx.Button(self, -1, "Graph Selected")
        self._sel_btn = wx.Button(self, -1, "Select All")
        self._unsel_btn = wx.Button(self, -1, "Unselect All")
        grid.Add(self._load_btn, (0, 0), (1, 1))
        grid.Add(self._graph_btn, (0, 1), (1, 1))
        grid.Add(self._sel_btn, (0, 2), (1, 1))
        grid.Add(self._unsel_btn, (0, 3), (1, 1))
        self._load_btn.Bind(wx.EVT_BUTTON, self.on_open)
        self._graph_btn.Bind(wx.EVT_BUTTON, self.on_graph)
        self._sel_btn.Bind(wx.EVT_BUTTON, self.on_selall)
        self._unsel_btn.Bind(wx.EVT_BUTTON, self.on_unselall)
        self.SetSizer(self.sizer)

        fontsz = wx.SystemSettings.GetFont(wx.SYS_SYSTEM_FONT).GetPixelSize()
        self.SetScrollRate(fontsz.x, fontsz.y)
        self.EnableScrolling(True, True)

    def on_clear(self, event=None):
        """ Clear all information from the form """
        self.checkboxes = {}
        self.dirname = ''
        self.filename = ''
        self.reader = None
        # TODO: Destroy all elements in the checkbox sizer

    def on_graph(self, event=None):
        """ Lazy String """
        if self.dirname == "" or self.filename == "":
            return
        checked = []
        points = {}
        # get the wanted points
        for key in self.checkboxes:
            if self.checkboxes[key].IsChecked():
                print("Key {} is checked".format(key))
                checked.append(key)
        # create an array for each data point wanted
        for name in checked:
            points[name] = []
        # iterate throgh the data
        with open(self.dirname+'/'+self.filename, 'r') as csvfile:
            self.reader = csv.DictReader(csvfile)
            for row in self.reader:
                # store the value of each point wanted to its corresponding array
                for name in points:
                    points[name].append(row[name])
        # plot each wanted array
        fig, axes = plt.subplots()
        for name in points:
            plt.plot(points[name], label=name)
        axes.legend(loc='upper right', shadow=True)
        plt.show()
        self.on_innersizechanged()

    def on_selall(self, event=None):
        """ Toggle All Checkboxes as selected """
        if self.checkboxes:
            for box in self.checkboxes:
                self.checkboxes[box].SetValue(True)

    def on_unselall(self, event=None):
        """ Unselect All Checkboxes """
        if self.checkboxes:
            for box in self.checkboxes:
                self.checkboxes[box].SetValue(False)

    def on_open(self, event=None):
        """ Open a file"""
        self.on_clear()
        dlg = wx.FileDialog(self, "Choose a file", self.dirname, "", "*.csv", wx.OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            self.filename = dlg.GetFilename()
            self.dirname = dlg.GetDirectory()
            with open(self.dirname+'/'+self.filename, 'r') as csvfile:
                self.reader = csv.DictReader(csvfile)
                col = 0
                for index, name in enumerate(self.reader.fieldnames):
                    # iterate through each field name and populate checkboxes
                    # with those fieldnames
                    if index > 0 and index % 5 == 0:
                        col += 1
                    self.checkboxes[name] = wx.CheckBox(self, label=name)
                    self.sizer.Add(self.checkboxes[name], (5 + (index % 10), col % 5))
        dlg.Destroy()

    def on_innersizechanged(self):
        """ set new size """
        width, height = self.sizer.GetMinSize()
        self.SetVirtualSize((width, height))

class MainFrame(wx.Frame):
    """ Main Frame Class """
    def __init__(self):
        wx.Frame.__init__(self, None, -1, 'EVOScan Logger')
        # self.control = wx.TextCtrl(self, style=wx.TE_MULTILINE)
        self.CreateStatusBar() # A Statusbar in the bottom of the window

        # Setting up the menu.
        filemenu = wx.Menu()
        menu_open = filemenu.Append(wx.ID_OPEN, "&Open", " Open a file to edit")
        menu_about = filemenu.Append(wx.ID_ABOUT, "&About", " Information about this program")
        menu_exit = filemenu.Append(wx.ID_EXIT, "E&xit", " Terminate the program")

        # Creating the menubar.
        menu_bar = wx.MenuBar()
        menu_bar.Append(filemenu, "&File") # Adding the "filemenu" to the MenuBar
        self.SetMenuBar(menu_bar)  # Adding the MenuBar to the Frame content.

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.mainwindow = MainWindow(self)
        self.sizer.Add(self.mainwindow, 1, wx.EXPAND)
        self.SetSizer(self.sizer)

        # Events.
        self.Bind(wx.EVT_MENU, self.mainwindow.on_open, menu_open)
        self.Bind(wx.EVT_MENU, self.on_exit, menu_exit)
        self.Bind(wx.EVT_MENU, self.on_about, menu_about)

    def on_about(self, event=None):
        """ Lazy String """
        # Create a message dialog box
        dlg = wx.MessageDialog(self, " Log Grapher for EVOScan logs", "About EVOGrapher")
        dlg.ShowModal() # Shows it
        dlg.Destroy() # finally destroy it when finished.

    def on_exit(self, event=None):
        """Lazy String"""
        self.Close(True)  # Close the frame.

def main():
    """ DA Main """
    wxapp = wx.App()
    frame = MainFrame()
    frame.Show(True)
    wxapp.MainLoop()

if __name__ == '__main__':
    main()
