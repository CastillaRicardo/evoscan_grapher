from appJar import gui
import csv
import matplotlib.pyplot as plt

filename = ""

def btnpress(btn):
    if btn == "Open Log":
        onopen()
    elif btn == "Graph Selected":
        ongraph()

def ongraph():
    """ Lazy String """
    global filename
    checkboxes = app.getProperties("Logging Values")
    points = {}
    # get the wanted points
    for key in checkboxes:
        if checkboxes[key]:
            print("Key {} is checked".format(key))
            # create an array for each data point wanted
            points[key] = []
    # iterate throgh the data
    with open(filename, 'r') as csvfile:
        print("Opened {}".format(filename))
        reader = csv.DictReader(csvfile)
        for row in reader:
            # store the value of each point wanted to its corresponding array
            for name in points:
                points[name].append(row[name])
    # plot each wanted array
    fig, axes = plt.subplots()
    for name in points:
        print("plotting")
        plt.plot(points[name], label=name)
    axes.legend(loc='upper right', shadow=True)
    plt.show()

def onopen():
    global filename
    filename = app.openBox("Select a log", fileTypes=[("Spreadsheet", '*.csv')])
    with open(filename, 'r') as csvfile:
        checkboxes = {}
        reader = csv.DictReader(csvfile)
        for name in reader.fieldnames:
            # iterate through each field name and populate checkboxes
            # with those fieldnames
            checkboxes[name] = False
        app.openScrollPane("logchecks")
        app.setProperties("Logging Values", checkboxes)
        app.stopScrollPane()

app=gui()

app.setGeometry(280,400)
app.addButtons(["Open Log", "Graph Selected"], btnpress)
app.startScrollPane("logchecks")
app.addProperties("Logging Values", {})
app.stopScrollPane()

app.go()
